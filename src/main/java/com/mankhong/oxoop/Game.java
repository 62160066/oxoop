/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.oxoop;

import java.util.Scanner;
/**
 *
 * @author W.Home
 */
public class Game {
    Player playerX;
    Player playerO;
    Table table;
    Scanner kb = new Scanner(System.in);
    public Game(){       
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX,playerO);    
    }
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    public void showTable(){
        table.showTable();
    }
    public boolean checkWin(){
        table.checkWin();
        if(table.isFinish == true){
            return true;
        }else{
            return false;
        }
    }
    public void inputPosition(){
        while(true){
            System.out.println("Please input Row Col: ");
            int row = kb.nextInt()-1;
            int col = kb.nextInt()-1;
            if(table.setRowCol(row,col )){
                table.turn();
                break;
            }
            System.out.println("Erroe: Position is not Empty");
            System.out.println("please input again");
            System.out.println("");
        }
        
        
    }
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getName()+" turn");
    }
    public void showWinner(){
        if(table.getWinner()==null){
            System.out.println("Draw !!!");
            System.out.println("Bye bye");
        }else{
            System.out.println("Winner is "+table.getWinner().getName()+" !!!");
            System.out.println("Bye bye");
            
        }
    }
    public void switchPlayer(){
        table.switchPlayer();
    }
    public void run(){
        showWelcome();       
        showTable();
        do{
           showTurn() ;
           inputPosition();
           showTable();
           table.checkWin();
           switchPlayer();
           
        }while(!table.isFinish == true);
        showWinner();
    }
}

