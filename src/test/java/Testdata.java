/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mankhong.oxoop.Player;
import com.mankhong.oxoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author W.Home
 */
public class Testdata {
    
    
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(x, table.getWinner());        
        
    } 
    @Test
    public void testRow2ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(x, table.getWinner());        
        
    }
    @Test
    public void testRow3ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(x, table.getWinner());        
        
    }
    @Test
    public void testCol1ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(x, table.getWinner());        
        
    }
    @Test
    public void testCol2ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(x, table.getWinner());        
        
    }
    @Test
    public void testCol3ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(x, table.getWinner());        
        
    }
    @Test
    public void testDiagonal1ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(x, table.getWinner());        
        
    }
    @Test
    public void testDiagonal2ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(x, table.getWinner());        
        
    }
    @Test
    public void testRow1ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(o, table.getWinner());        
        
    } 
    @Test
    public void testRow2ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(o, table.getWinner());        
        
    }
    @Test
    public void testRow3ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(o, table.getWinner());        
        
    }
    @Test
    public void testCol1ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(o, table.getWinner());        
        
    }
    @Test
    public void testCol2ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(o, table.getWinner());        
        
    }
    @Test
    public void testCol3ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(o, table.getWinner());        
        
    }
    @Test
    public void testDiagonal1ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(o, table.getWinner());        
        
    }
    @Test
    public void testDiagonal2ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(o, table.getWinner());        
        
    }
    @Test
    public void testNoWin(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);        
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.getIsFinish());
        assertEquals(null, table.getWinner());  
    }
    @Test
    public void testDraw(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);        
        table.setRowCol(0, 0);
        table.turn();
        table.switchPlayer();
        table.setRowCol(1, 1);
        table.turn();
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.turn();
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.turn();
        table.switchPlayer();
        table.setRowCol(1, 2);
        table.turn();
        table.switchPlayer();
        table.setRowCol(1, 0);
        table.turn();
        table.switchPlayer();
        table.setRowCol(2, 0);
        table.turn();
        table.switchPlayer();
        table.setRowCol(2, 1);
        table.turn();
        table.switchPlayer();
        table.setRowCol(2, 2);
        table.turn();        
        table.checkWin();
        assertEquals(true, table.getIsFinish());
        assertEquals(null, table.getWinner());  
        assertEquals(9, table.getTurn());  
        
    }
}
